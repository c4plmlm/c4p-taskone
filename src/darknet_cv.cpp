#include <darknet_cv.h>

using namespace cv;
using namespace std;

extern "C"
{
    IplImage *image_to_ipl(image im)
    {
        int x, y, c;
        IplImage *disp = cvCreateImage(cvSize(im.w, im.h), IPL_DEPTH_8U, im.c);
        int step = disp->widthStep;
        for (y = 0; y < im.h; ++y)
        {
            for (x = 0; x < im.w; ++x)
            {
                for (c = 0; c < im.c; ++c)
                {
                    float val = im.data[c * im.h * im.w + y * im.w + x];
                    disp->imageData[y * step + x * im.c + c] = (unsigned char)(val * 255);
                }
            }
        }
        return disp;
    }

    image ipl_to_image(IplImage *src)
    {
        int h = src->height;
        int w = src->width;
        int c = src->nChannels;
        image im = make_image(w, h, c);
        unsigned char *data = (unsigned char *)src->imageData;
        int step = src->widthStep;
        int i, j, k;

        for (i = 0; i < h; ++i)
        {
            for (k = 0; k < c; ++k)
            {
                for (j = 0; j < w; ++j)
                {
                    im.data[k * w * h + i * w + j] = data[i * step + j * c + k] / 255.;
                }
            }
        }
        return im;
    }

    image mat_to_image(Mat m)
    {
        IplImage ipl = m;
        image im = ipl_to_image(&ipl);
        rgbgr_image(im);
        return im;
    }

    Mat image_to_mat(image im)
    {
        image copy = copy_image(im);
        constrain_image(copy);
        if (im.c == 3)
            rgbgr_image(copy);

        IplImage *ipl = image_to_ipl(copy);
        Mat m = cvarrToMat(ipl, true);
        cvReleaseImage(&ipl);
        free_image(copy);
        return m;
    }
}

void writeVectorOfVector(FileStorage &fs, string name, vector<vector<Point2i>> &data)
{
    fs << name;
    fs << "{";
    for (unsigned int i = 0; i < data.size(); i++)
    {
        fs << name + "_" + to_string(i);
        vector<Point2i> tmp = data[i];
        fs << tmp;
    }
    fs << "}";
}

int readVectorOfVector(FileStorage &fns, string name, vector<vector<Point2i>> &data)
{
    data.clear();
    FileNode fn = fns[name];
    if (fn.empty())
    {
        return 0;
    }

    FileNodeIterator current = fn.begin(), it_end = fn.end();
    for (; current != it_end; ++current)
    {
        vector<Point2i> tmp;
        FileNode item = *current;
        item >> tmp;
        data.push_back(tmp);
    }
    return 1;
}

int readFloat(cv::FileStorage &fs, std::string name, float &data)
{
    FileNode fn = fs[name];
    if (fn.empty())
    {
        return 0;
    }

    fn >> data;
    return 1;
}