#include <iostream>
#include <string>
#include <darknet.h>
#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include "darknet_cv.h"

using namespace std;
using namespace cv;

struct someobject
{
    int id;
    Rect2d box;
    Point2d mv;
    int frames;
    int revived;
    Ptr<Tracker> tracker;
};


int main()
{
    // Path to configuration file
    static char *cfg_file = const_cast<char *>("cfg/v3single.cfg");
    // Path to weight file
    static char *weight_file = const_cast<char *>("v3single.weights");
    // Path to a file describing classes names
    static char *names_file = const_cast<char *>("data/v3single.names");
    // Define thresholds
    float thresh = 0.25;
    float hier_thresh = 0.5;
    float nms = 0.45;
    float areathresh = 10;

    // Paths to video, its config and display resize factor
    string videofile = "/home/ivan/Videos/code4piter2019/Novgorod_2019-04-04-09_00_00.mp4";
    string viewconfigfile = "view-config_nov.yml";
    float displayresize = 0.25;

    // Open the video source
    VideoCapture cap = VideoCapture(videofile);
    if (!cap.isOpened())
    {
        cout << "cannot open video!" << endl;
        return -1;
    }

    // Load Darknet network
    network *net = load_network(cfg_file, weight_file, 0);
    set_batch_network(net, 1);

    Mat captured, resized, resized_display;
    // Load one frame to get image dims
    cap >> captured;

    // Set scaling ratios for the net
    double scaleratiow = (double)net->w / (double)captured.cols;
    double scaleratioh = (double)net->h / (double)captured.rows;

    // Read restricted areas from file
    vector<vector<Point>> restricted, restricted_orig, restricted_display;
    FileStorage fs(viewconfigfile, FileStorage::READ);
    readVectorOfVector(fs, "restrict", restricted_orig);
    readFloat(fs, "areathresh", areathresh);
    fs.release();

    // Resize restricted areas for detection and for display (quality code right here)
    for (unsigned int i = 0; i < restricted_orig.size(); i++)
    {
        restricted.push_back(vector<Point>());
        restricted_display.push_back(vector<Point>());
        for (auto &point : restricted_orig[i])
        {
            restricted[i].push_back(Point(point.x * scaleratiow, point.y * scaleratioh));
            restricted_display[i].push_back(point * displayresize);
        }
    }

    // Container for tracked objects
    vector<someobject> objects;
    int maxid = 0;

    TrackerKCF::Params trackerparams;
    trackerparams.sigma = 1.0f;

    // Number of classes
    size_t classes = 0;
    char **labels = get_labels(names_file);
    while (labels[classes] != nullptr)
    {
        classes++;
    }
    cout << "Num of Classes " << classes << endl;

    while (1)
    {
        cout << "-------------------------------------------" << endl;
        double timestart = what_time_is_it_now();

        // Get the next image
        cap >> captured;
        if (captured.rows == 0 || captured.cols == 0)
            break;

        // Resize image to fit net dims
        resize(captured, resized, Size(), scaleratiow, scaleratioh, INTER_LINEAR);

        image im = mat_to_image(resized);

        // Get actual data associated with image
        float *frame_data = im.data;

        // Do prediction
        double time = what_time_is_it_now();
        network_predict(net, frame_data);
        cout << "predicted in " << (what_time_is_it_now() - time) << " sec." << endl;

        // Get number fo predicted classes (objects)
        int num_boxes = 0;
        detection *detections = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &num_boxes);

        // Do NMS on predictions
        do_nms_sort(detections, num_boxes, classes, nms);

        // Update positions predicted by trackers
        for (auto currobject = objects.begin(); currobject != objects.end();)
        {
            // Remove object if it had no detections in last n frames
            if ((*currobject).frames > 1500)
            {
                cout << "Tracker " << (*currobject).id << " is old and will be removed" << endl;
                currobject = objects.erase(currobject);
            }
            else
            {
                // Track object into this frame
                Point2d oldpos = Point2d((*currobject).box.x, (*currobject).box.y);
                bool ret = (*currobject).tracker->update(resized, (*currobject).box);
                if (!ret)
                {
                    cout << "Tracker " << (*currobject).id << " has failed and will be removed" << endl;
                    currobject = objects.erase(currobject);
                    
                }
                else
                {
                    // Check if tracked area is in bounds
                    bool isInBounds = true;
                    for (auto &area : restricted)
                    {
                        if (pointPolygonTest(area, Point2f((*currobject).box.x + (*currobject).box.width / 2, (*currobject).box.y + (*currobject).box.height / 2), false) > 0)
                        {
                            isInBounds = false;
                            break;
                        }
                    }
                    if (!isInBounds)
                    {
                        cout << "Tracker " << (*currobject).id << " is OOB and will be removed" << endl;
                        currobject = objects.erase(currobject);
                    }
                    else
                    {
                        (*currobject).mv.x = (*currobject).box.x - oldpos.x;
                        (*currobject).mv.y = (*currobject).box.y - oldpos.y;
                        (*currobject).frames++;
                        (*currobject).revived = 0;
                        ++currobject;
                    }
                }
            }
        }

        // Iterate over predicted classes, print information and update objects
        for (int8_t i = 0; i < num_boxes; ++i)
        {
            for (uint8_t j = 0; j < classes; ++j)
            {
                if (detections[i].prob[j] > thresh)
                {
                    // Convert darknet bounding box to Rect2d
                    int w = detections[i].bbox.w * resized.cols;
                    int h = detections[i].bbox.h * resized.rows;
                    int x = detections[i].bbox.x * resized.cols - w / 2;
                    int y = detections[i].bbox.y * resized.rows - h / 2;
                    Rect2d detected = Rect(x, y, w ,h);
                    int area = detected.area();

                    // First, see if center box is in a restricted area
                    bool isInBounds = true;
                    for (auto &area : restricted)
                    {
                        if (pointPolygonTest(area, Point2f(detections[i].bbox.x * resized.cols, detections[i].bbox.y * resized.rows), false) > 0)
                        {
                            isInBounds = false;
                            break;
                        }
                    }
                    if (isInBounds && area > areathresh)
                    {
                        // Iterate over trackers to see if any of them matches this detection
                        bool found = false;
                        for (unsigned i = 0; i < objects.size(); i++)
                        {
                            if (objects[i].frames > 0)
                            {
                                Rect2d tracked = objects[i].box;
                                double trackedarea = tracked.area();
                                double detectedarea = detected.area();
                                double intersection = (tracked & detected).area();
                                double smallerarea = (trackedarea > detectedarea) ? detectedarea : trackedarea;
                                double iou = (tracked & detected).area() / (tracked | detected).area();
                                double ioa = intersection / smallerarea;
                                if (ioa > 0.9)
                                {
                                    // If one box is contained within another, leave the larger one
                                    cout << "Tracker " << objects[i].id << " is merged with a detection" << endl;
                                    objects[i].box = detected;
                                    objects[i].tracker->init(resized, objects[i].box);
                                    objects[i].revived = 0;
                                    // objects[i].frames++;
                                    found = true;
                                    break;
                                }
                                if (iou > 0.25)
                                {
                                    // If a tracked box matches this detected box, reset tracker to the detection
                                    cout << "Tracker " << objects[i].id << " is re-detected" << endl;
                                    objects[i].box = detected;
                                    objects[i].tracker->init(resized, objects[i].box);
                                    objects[i].revived = 0;
                                    // objects[i].frames++;
                                    found = true;
                                    break;
                                }
                            }
                        }

                        // If no existing object matched this detection, add a new one and attach a tracker
                        if (!found)
                        {
                            someobject newdet;
                            newdet.id = maxid++;
                            newdet.box = detected;
                            newdet.frames = 0;
                            newdet.tracker = TrackerKCF::create(trackerparams);
                            newdet.tracker->init(resized, detected);
                            newdet.mv = Point2d(0, 0);
                            newdet.revived = 0;
                            objects.push_back(newdet);
                            cout << "Tracker " << newdet.id << " has been created" << endl;
                        }
                    }
                }
            }
        }

        free_detections(detections, num_boxes);

        // Also resize input for display and draw boxes over it
        resize(captured, resized_display, Size(), displayresize, displayresize, INTER_NEAREST);
        for (auto &obj : objects)
        {
            Scalar clr = Scalar(0, 255, 0);
            Rect adjustedrect = Rect(obj.box.x / scaleratiow * displayresize, obj.box.y / scaleratioh * displayresize, obj.box.width / scaleratiow * displayresize, obj.box.height / scaleratioh * displayresize);
            rectangle(resized_display, adjustedrect, clr);
        }

        for (unsigned int i = 0; i < restricted_display.size(); i++)
        {
            drawContours(resized_display, restricted_display, i, Scalar(255, 0, 0));
        }

        imshow("result", resized_display);
        int retkey = waitKey(1);

        free_image(im);
        if (retkey > 0)
            break;

        double timestop = what_time_is_it_now();
        cout << "total frame time " << (timestop - timestart) << " sec., " << 1.0f / (timestop - timestart) << " FPS" << endl;
    }

    free(labels);

    return 0;
}
