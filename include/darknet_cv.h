#pragma once

#include <string>
#include <iostream>

#include <darknet.h>
#include "opencv2/opencv.hpp"

extern "C"
{
    IplImage *image_to_ipl(image im);
    image ipl_to_image(IplImage *src);
    image mat_to_image(cv::Mat m);
    cv::Mat image_to_mat(image im);
}

void writeVectorOfVector(cv::FileStorage &fs, std::string name, std::vector<std::vector<cv::Point2i>> &data);
int readVectorOfVector(cv::FileStorage &fns, std::string name, std::vector<std::vector<cv::Point2i>> &data);
int readFloat(cv::FileStorage &fs, std::string name, float &data);